//Declare dependencies and model
const Task = require("../models/tasks");
const express = require("express");
const router = express.Router(); //to handle routing

//1) CREATE
router.post("/", async (req, res) => {
	const task = new Task(req.body);
	//Commented out for async operation//
	// task.save()
	// 	.then(() => {
	// 		res.send(task)
	// 	})
	// 	.catch((e) => {
	// 		res.status(400).send(e)
	// 	})
	//Commented out for async operation//
	try {
		await task.save(); 
		res.send(task);
	} catch(e) {
		res.status(400).send(e)
	}
});

//2) GET ALL
router.get("/", async (req, res) => {
	//Commented out for async operation//
	// Task.find()
	// 	.then((tasks) => {
	// 		return res.status(200).send(tasks)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//
	try {
		const tasks = await Task.find();
		return res.status(200).send(tasks); //if no return, it will execute the succeeding lines in the command line
	} catch(e) {
		return res.status(404).send(e)
	}
});

//3) GET ONE
router.get("/:id", async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Task.findById(_id)
	// 	.then((task) => {
	// 		if(!task) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(task)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//	
	try {
		const task = await Task.findById(_id);
		if(!task) {
			return res.status(404).send("Task doesn't exist!");
		}
		res.send(task)
	} catch(e) {
		return res.status(500).send(e);
	}
});

//4) UPDATE ONE
router.patch("/:id", async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Task.findByIdAndUpdate(_id, req.body, { new: true })
	// 	.then((task) => {
	// 		if(!task) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(task)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//
	try {
		const task = await Task.findByIdAndUpdate(_id, req.body, { new: true });
		if(!task) {
			return res.status(404).send("Task doesn't exist!");
		}
		res.send(task);
	} catch(e) {
		return res.status(500).send(e);
	}
});

//5) DELETE ONE
router.delete("/:id", async (req, res) => {
	const _id = req.params.id;
	//Commented out for async operation//
	// Task.findByIdAndDelete(_id)
	// 	.then((task) => {
	// 		if(!task) {
	// 			return res.status(404).send(e)
	// 		}

	// 		return res.send(task)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})
	//Commented out for async operation//	
	try {
		const task = await Task.findByIdAndDelete(_id);
		if(!task) {
			return res.status(404).send("Task doesn't exist");
		}
		res.send(task);
	} catch(e) {
		return res.status(500).send(e);
	}
});

module.exports = router;